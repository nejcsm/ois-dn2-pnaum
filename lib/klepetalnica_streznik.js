var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var socketGledeNaVzdevki = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var geslo = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki, socketGledeNaVzdevki);
    pridruzitevKanalu(socket, 'Skedenj', "");
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajPosredovanjeZasebnihSporocil(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki, socketGledeNaVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajPridruzitevKanaluZGeslom(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabniki', function() {
      var kanal = trenutniKanal[socket.id];
      var uporabnikiNaKanalu = io.sockets.clients(kanal);
      var uporabnikiNaKanaluPovzetek = '';
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ',';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
      socket.emit('uporabniki', uporabnikiNaKanaluPovzetek);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki, socketGledeNaVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki, soket) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  soket[vzdevek] = socket.id;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal, passwd) {
  socket.join(kanal);
  geslo[kanal] = passwd;
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki, socketGledeNaVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        socketGledeNaVzdevki[vzdevek] = socket.id;
        delete socketGledeNaVzdevki[uporabljeniVzdevki[prejsnjiVzdevekIndeks]];
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPosredovanjeZasebnihSporocil(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('zasebnoSporocilo', function (sporocilo) {
    var prejem = sporocilo.prejemnik;
    if (uporabljeniVzdevki.indexOf(prejem) !== -1 && socket.id !== socketGledeNaVzdevki[prejem]){
      io.sockets.socket(socketGledeNaVzdevki[prejem]).emit('sporocilo',{
        besedilo: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): ' + sporocilo.besedilo
      });
      io.sockets.socket(socket.id).emit('sporocilo',{
        besedilo: "(zasebno za " + prejem + "): " + sporocilo.besedilo
      });
    }else {
      io.sockets.socket(socket.id).emit('sporocilo',{
        besedilo: "Sporočila \"" + sporocilo.besedilo + "\" uporabniku z vzdevkom \"" + prejem + "\" ni bilo mogoče posredovati."
      });
    }
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    if(preveriUstreznostGesla(kanal.novKanal, "").localeCompare("prav") === 0){
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal, "");
    }else{
      socket.emit('sporocilo', {besedilo: "Pridružitev v kanal " + kanal.novKanal + " ni bilo uspešno, ker je geslo napačno!"});
    }
  });
}

function obdelajPridruzitevKanaluZGeslom(socket) {
  socket.on('pridruzitevZahtevaGeslo', function(kanal) {
    if(preveriUstreznostGesla(kanal.novKanal, kanal.passwd).localeCompare("prav") === 0){
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal, kanal.passwd);
    }else if(preveriUstreznostGesla(kanal.novKanal, kanal.passwd).localeCompare("narobe") === 0){
      socket.emit('sporocilo', {besedilo: "Pridružitev v kanal " + kanal.novKanal + " ni bilo uspešno, ker je geslo napačno!"});
    }else if(preveriUstreznostGesla(kanal.novKanal, kanal.passwd).localeCompare("brezgesla") === 0){
      socket.emit('sporocilo', {besedilo: "Izbrani kanal " + kanal.novKanal + " je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom."});
    }
  });
}

function preveriUstreznostGesla(kanal, passwd) {
  var kanalObstaja = false;
  var kanali = io.sockets.manager.rooms;
  for(var k in kanali) {
    k = k.substring(1, k.length);
    if(kanal.localeCompare(k) === 0){
      kanalObstaja = true;
    }
  }
  if(kanalObstaja){
    if(geslo[kanal].localeCompare(passwd) === 0){
      return "prav";
    }else if(geslo[kanal] === "" && passwd !== ""){
      return "brezgesla";
    }else{
      return "narobe";
    }
  }
  return "prav";
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete socketGledeNaVzdevki[uporabljeniVzdevki[vzdevekIndeks]];
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}