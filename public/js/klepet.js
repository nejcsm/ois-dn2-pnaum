var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.posljiZasebnoSporocilo = function(prejemnik, besedilo) {
  var sporocilo = {
    prejemnik: prejemnik,
    besedilo: besedilo
  };
  this.socket.emit('zasebnoSporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal, passwd) {
  if(passwd.localeCompare("") === 0){
    this.socket.emit('pridruzitevZahteva', {
      novKanal: kanal
    });
  }else{
    this.socket.emit('pridruzitevZahtevaGeslo', {
      novKanal: kanal,
      passwd: passwd
    });
  }
};

Klepet.prototype.spremeniKanal = function(kanal, passwd) {
  if(passwd.localeCompare("") === 0){
    this.socket.emit('pridruzitevZahteva', {
      novKanal: kanal
    });
  }else{
    this.socket.emit('pridruzitevZahtevaGeslo', {
      novKanal: kanal,
      passwd: passwd
    });
  }
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var besedeAll = "";
      for(var i=0; i<besede.length; i++){
        besedeAll += besede[i];
        if(i !== besedeAll.length){
          besedeAll += " ";
        }
      }
      var besedeAllSplitane = "";
      if((besedeAllSplitane = besedeAll.split('"')).length !== 5){
        var kanal = besede.join(' ');
        this.spremeniKanal(kanal, "");
      }else{
        if(besedeAllSplitane[2] === " "){
          var kanalZGeslom = besedeAllSplitane[1];
          var passwd = besedeAllSplitane[3];
          this.spremeniKanal(kanalZGeslom, passwd);
        }else{
          sporocilo = 'Neznan ukaz.';
        }
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var vseBesede ="";
      for(var i=0; i<besede.length; i++){
        vseBesede += besede[i];
        if(i !== (besede.length-1)){
          vseBesede += " ";
        }
      }
      var prejemnik = "";
      if(vseBesede.charAt(0) !== '"' || vseBesede.charAt(vseBesede.length-1) !== '"' || vseBesede.match(/\"/g).length < 4){
        sporocilo = 'Neznan ukaz.';
      }else{
        vseBesede = vseBesede.split('"');
        prejemnik = vseBesede[1];
        if(vseBesede[2] !== ' '){
          sporocilo = 'Neznan ukaz';
        }else{
          var celoSporocilo = "";
          for(var i=3; i<vseBesede.length-1; i++){
            celoSporocilo += vseBesede[i];
            if(i !== (vseBesede.length-2)){
              celoSporocilo += '"';
            }
          }
          this.posljiZasebnoSporocilo(prejemnik, celoSporocilo);
        }
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};