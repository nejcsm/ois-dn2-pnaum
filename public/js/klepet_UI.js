function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function klientSporociloHtml(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function prepreciXSSNapad(sporocilo){
  var lt = /</g, 
      gt = />/g;
  sporocilo = sporocilo.replace(lt, "&lt;").replace(gt, "&gt;");
  
  return sporocilo;
}

function filterBesed(sporocilo, banned){
  for(var i = 0; i<banned.length; i++){
    var zamenjava = "";
    var j =0;
    while(j<banned[i].length){
      if(banned[i].charAt(j) !== " "){
        zamenjava += "*";
      }else{
        zamenjava += " ";
      }
      j++;
    }
    var re = new RegExp("\\b"+banned[i]+"\\b", "gi");
    sporocilo = sporocilo.replace(re, zamenjava);
  }
  
  return sporocilo;
}

function zamenjajSmeskov(sporocilo) {
  var url = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/";
  
  sporocilo = sporocilo.replace(/;\)/g, '<img src="' + url + 'wink.png" />')
  .replace(/:\)/g, '<img src="' + url + 'smiley.png" />')
  .replace(/\(y\)/g, '<img src="' + url + 'like.png" />')
  .replace(/:\*/g, '<img src="' + url + 'kiss.png" />')
  .replace(/:\(/g, '<img src="' + url + 'sad.png" />');
  
  return sporocilo;
}

function bannedWordsList(){
     var ban = null;
     var povezava = "/swearWords.csv";
     $.ajax({
        url: povezava,
        type: 'get',
        dataType: 'text',
        async: false,
        success: function(data) {
            ban = data.split(",");
        } 
     });
     return ban;
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = prepreciXSSNapad(sporocilo);
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    if(sporocilo.charAt(1) == 'z'){
      var banned = bannedWordsList();
      sporocilo = filterBesed(sporocilo, banned);
      sporocilo = zamenjajSmeskov(sporocilo);
    }
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else{
    var bannedWords = bannedWordsList();
    sporocilo = filterBesed(sporocilo, bannedWords);
    sporocilo = zamenjajSmeskov(sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text().split(" @ ")[1], sporocilo);
    $('#sporocila').append(klientSporociloHtml(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      var channel = $('#kanal').text().split(" @ ")[1];
      $('#kanal').text(rezultat.vzdevek+" @ "+channel);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    var usr = $('#kanal').text().split(" @ ")[0];
    $('#kanal').text(usr+" @ "+rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    var uporabnik = uporabniki.split(",");
    for(var i = 0; i<uporabnik.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabnik[i]));
    }
  });
  
  setInterval(function() {
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});